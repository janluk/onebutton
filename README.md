# One Button library (Tested on STM32 NUCLEO-F411RE)

With this library you can call up four independent states with a single button, such as:
- One click 
- Double click 
- Long Press Start (repeat itself) 
- Long Press Stop


# [1] **Initialize** 
```
	OneButton_t BlueKey;
	OneButton_t BlueKey_2;

	OneButtonInit(&BlueKey,B1_GPIO_Port,B1_Pin);
	OneButtonInit(&BlueKey_2,B2_GPIO_Port,B2_Pin);

```


# [2] **Registering CallBack**
```

// for BluKey
  OneButtonCallbackOneClick(&BlueKey,TurnOnLed);
  OneButtonCallbackDoubleClick(&BlueKey,TurnOffLed);
  OneButtonCallbackLongPressStart(&BlueKey,ToggleLed); // the function will repeat itself until you release the button
  OneButtonCallbackLongPressStop(&BlueKey,TurnOffLed); // the function will be performed after releasing the long press

  
// for BluKey_2
  OneButtonCallbackOneClick(&BluKey_2,TurnOnLed);
  OneButtonCallbackDoubleClick(&BluKey_2,TurnOffLed);
  OneButtonCallbackLongPressStart(&BluKey_2,ToggleLed);
  OneButtonCallbackLongPressStop(&BluKey_2,TurnOffLed);
  
```
# [3] **Add tasks to the main loop**
```
// for BluKey	
	OneButtonTask(&BlueKey);
	
// for BluKey_2	
	OneButtonTask(&BlueKey_2);

```

# **Default library settings:**
	Time of Debounce			10
	Time of Double Click		350
	Time of Long Press Start	700
	Time of Long Press Ticking	500


# **Modification of standard library settings**

### * The time it takes for the debounce button
```
	// for BluKey
	OneButtonSetTimerDebounce(&BlueKey, 20);

	// for BluKey_2
	OneButtonSetTimerDebounce(&BluKey_2, 20);

```

### * Time for a double click
```
	// for BluKey
		OneButtonSetTimerDoubleClick(&BlueKey, 400);

	// for BluKey_2
		OneButtonSetTimerDoubleClick(&BluKey_2, 400);

```
### * The time it takes for the start long press of button
```
	// for BluKey
		OneButtonSetTimerLongPressStart(&BlueKey, 800);

	// for BluKey_2
		OneButtonSetTimerLongPressStart(&BluKey_2, 800);
```

### * Repeat time of a long press of button
```
	// for BluKey	
		OneButtonSetTimerLongPressTick(&BlueKey, 150);

	// for BluKey_2
		OneButtonSetTimerLongPressTick(&BluKey_2, 150);
```

# **Simple porting**
If you would to use the library on another microrontroler, change the definitions in the OneButton.h file
```
	#include "main.h"

	#define OB_READ_PIN() 			HAL_GPIO_ReadPin(Btn->GpioPort, Btn->GpioPin)
	#define OB_GET_TICK() 			HAL_GetTick()
	#define OB_BUTTON_PRESSED 		GPIO_PIN_RESET
	#define OB_BUTTON_NOT_PRESSED 	GPIO_PIN_SET
```
# **Simple changing CallBack**
If you would to use another CallBack, change typedef in the OneButton.h file
 ```
	typedef void(*CallBackFunOneClick_t)(void);
	typedef void(*CallBackFunDoubleClick_t)(void);
	typedef void(*CallBackFunLongPressStart_t)(void);
	typedef void(*CallBackFunLongPressStop_t)(void);
```
